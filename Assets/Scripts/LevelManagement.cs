using UnityEngine;
using Zenject;

public class LevelManagement : MonoBehaviour
{
    [SerializeField] private Level[] levelPrefabs;

    [Inject] private DiContainer diContainer;

    private Level currentLevel;
    private int currentlevelIndex;
    private int lastLevelPassed;

    public Level CurrentLevel => currentLevel;


    private void Start()
    {
        if (PlayerPrefs.HasKey("lastLevelPassed"))
        {
            lastLevelPassed = PlayerPrefs.GetInt("lastLevelPassed");
        }
        else lastLevelPassed = -1;
    }

    private void LoadLevel(int level)
    {
        currentLevel = diContainer.InstantiatePrefab(levelPrefabs[level]).GetComponent<Level>();

        currentLevel.EnemySpawner.gameObject.SetActive(true);
        currentLevel.EnemySpawner.StartGame();
        currentlevelIndex = level;
        lastLevelPassed = level - 1;
    }

    public void LoadNextLevel() 
    {
        lastLevelPassed = currentlevelIndex;
        Destroy(currentLevel.gameObject);
        LoadLevel((++currentlevelIndex) % levelPrefabs.Length);
    }

    public void StartNewGame()
    {
        LoadLevel(0);
    }

    public void Continue()
    {
        LoadLevel(lastLevelPassed + 1);
    }

    private void OnApplicationQuit()
    {
        PlayerPrefs.SetInt("lastLevelPassed", lastLevelPassed);
    }
}
