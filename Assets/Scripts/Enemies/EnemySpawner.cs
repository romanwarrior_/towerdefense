using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class EnemySpawner : MonoBehaviour
{
    [SerializeField] private Waypoints[] paths;
    [SerializeField] private GameObject _enemyPrefab;
    [SerializeField] private Transform _spawnPoint;
    [SerializeField] private int[] _wavesVolume;
    [SerializeField] private float _spawnDelay;

    public List<Enemy> Enemies;
    public int Wave => _currentWave;

    [SerializeField] private int _currentWave;

    [Inject] private DiContainer diContainer;
    [Inject] private GameEvents events;


    private void OnEnable()
    {
        events.OnGameStarted += StartGame;
        events.OnEnemyDied += OnEnemyDied;

        StartGame();
    }

    public void SpawnNextWave()
    {
        _currentWave++;
        StartCoroutine(SpawnWave(_wavesVolume[_currentWave]));
    }

    private IEnumerator SpawnWave(int enemiesCount)
    {
        for (int i = 0; i < enemiesCount; i++)
        {
            SpawnEnemy();
            yield return new WaitForSeconds(_spawnDelay);
        }
    }

    private void SpawnEnemy()
    {
        var enemy = diContainer.InstantiatePrefab(_enemyPrefab).GetComponent<Enemy>();
        enemy.transform.position = _spawnPoint.position;
        enemy.Init(paths[Random.Range(0,paths.Length)]);
        Enemies.Add(enemy);
    }

    public void StartGame()
    {
        _currentWave = -1;
        Enemies = new List<Enemy>();
    }

    private void OnEnemyDied(Enemy enemy)
    {
        Enemies.Remove(enemy);
        if (Enemies.Count == 0 && InputHandler.HasControls)
        {
            events.OnWaveCleared(Wave);
        }
    }
}
