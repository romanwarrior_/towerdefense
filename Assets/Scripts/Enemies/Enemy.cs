using UnityEngine;
using UnityEngine.Events;
using Zenject;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Transform _model;
    [SerializeField] private float _speed;
    [SerializeField] private float _damage;
    [SerializeField] private float _maxHealth;

    public UnityAction<Enemy> OnEnemyDied;
    public float Health => _health;
    public float MaxHealth => _maxHealth;

    [Inject] private GameEvents events;
    [Inject] private LevelManagement levelManagement;
    private Headquarters hq;

    private Transform _currentWaypoint;
    private Waypoints path;
    private int _currentWaypointIndex;

    private float _health;
    private bool _alive;

    private void Start()
    {
        transform.position = path.Points[0].position;
        _currentWaypointIndex = 1;
        _currentWaypoint = path.Points[_currentWaypointIndex];
        _health = _maxHealth;
        _alive = true;

        events.OnGameFinished += Die;
        hq = levelManagement.CurrentLevel.HQ;
    }

    public void Init(Waypoints path)
    {
        this.path = path;
    }

    private void Update()
    {
        if (!_alive) return;
        Move();
        CheckDistance();
    }

    private void CheckDistance()
    {
        float distance = Vector3.Distance(transform.position, _currentWaypoint.position);
        if (distance <= 0.1f)
        {
            ChangeWaypoint();
        }
    }

    private void Move()
    {
        Vector3 moveDirection = _currentWaypoint.position - transform.position;
        transform.Translate(moveDirection.normalized * Time.deltaTime * _speed, Space.World);

        Vector3 lookRotation = Quaternion.LookRotation(moveDirection).eulerAngles;
        _model.eulerAngles = lookRotation;
    }

    public void GetDamage(float value)
    {
        _health -= value;
        if (_health <= 0) Die();
    }

    private void Die(bool _ = true)
    {
        if (!_alive) return;

        events.OnEnemyDied?.Invoke(this);
        _alive = false;
        Destroy(gameObject);
    }

    private void ChangeWaypoint()
    {
        _currentWaypointIndex++;
        if (_currentWaypointIndex >= path.Points.Length)
        {
            hq.GetDamage(_damage);
            Die();
            return;
        }
        _currentWaypoint = path.Points[_currentWaypointIndex];
    }
}
