using UnityEngine;
using Zenject;

public class InputHandler : MonoBehaviour
{
    public static bool HasControls => InputEnabled;

    private static bool InputEnabled = true;

    [Inject] private GameEvents events;

    private void Start()
    {
        events.OnGameFinished += DisableInput;
        events.OnGameStarted += EnableInput;
    }

    private void Update()
    {
        if (!InputEnabled) return;

        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            int layerMask = 1 << 3;
            if (Physics.Raycast(ray, out hit, 999, layerMask))
            {
                if (hit.collider.TryGetComponent(out ClickableCell cell))
                {
                    HandleCellClick(cell);
                }
            }
        }
    }

    private void HandleCellClick(ClickableCell cell)
    {
        events.OnCellClicked(cell);
    }

    public static void EnableInput()
    {
        InputEnabled = true;
    }
    public static void DisableInput(bool _ = true)
    {
        InputEnabled = false;
    }
}
