using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Level : MonoBehaviour
{
    [SerializeField] private EnemySpawner enemySpawner;
    [SerializeField] private Headquarters hq;

    public EnemySpawner EnemySpawner => enemySpawner;
    public Headquarters HQ => hq;


}
