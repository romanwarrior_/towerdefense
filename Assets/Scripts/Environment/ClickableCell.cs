using UnityEngine;
using Zenject;

public class ClickableCell : MonoBehaviour
{
    [Inject] private GameEvents events;

    public Tower CurrentTower => _currentTower;

    private Tower _currentTower;

    private void Start()
    {
        events.OnGameStarted += Restart;
    }

    public void OnTowerBuild(Tower tower)
    {
        _currentTower = tower;
    }

    private void Restart()
    {
        _currentTower = null;
    }
}
