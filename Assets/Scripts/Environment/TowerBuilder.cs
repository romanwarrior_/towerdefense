using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class TowerBuilder : MonoBehaviour
{
    [SerializeField] private GameObject[] _towerPrefabs;
    [SerializeField] private float[] _towerBuildOffsetY;

    private static List<Tower> _towers;

    [Inject] private GameEvents events;
    [Inject] private DiContainer diContainer;

    private void Start()
    {
        _towers = new List<Tower>();
        Restart();
        events.OnGameStarted += Restart;
    }

    public void BuildTower(ClickableCell cell, int towerIndex)
    {
        var newTower = diContainer.InstantiatePrefab(_towerPrefabs[towerIndex]).GetComponent<Tower>();
        newTower.transform.position = cell.transform.position + Vector3.up * _towerBuildOffsetY[towerIndex];
        cell.OnTowerBuild(newTower);
        _towers.Add(newTower);
    }

    private void Restart()
    {
        foreach(var tower in _towers)
        {
            Destroy(tower.gameObject);
        }
        _towers = new List<Tower>();
    }
}
