using UnityEngine;
using UnityEngine.Events;

public class GameEvents : MonoBehaviour
{
    public UnityAction<Enemy> OnEnemyDied;
    public UnityAction OnGameStarted;
    public UnityAction<int> OnWaveCleared;
    public UnityAction<bool> OnGameFinished;
    public UnityAction<ClickableCell> OnCellClicked;

}
