using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartWavePanel : MonoBehaviour
{
    [SerializeField] private LevelManagement levelManagement;

    public void StartNewWave()
    {
        levelManagement.CurrentLevel.EnemySpawner.SpawnNextWave();
        gameObject.SetActive(false);
    }
}
