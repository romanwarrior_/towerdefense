using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class HeadquartersSlider : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [Inject] private LevelManagement levelManagement;
    private Headquarters hq;

    private void Start()
    {
        hq = levelManagement.CurrentLevel.HQ;
        _slider.maxValue = hq.MaxHp;
    }

    private void Update()
    {
        _slider.value = hq.Hp;
    }
}
