using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class IngameUIManager : MonoBehaviour
{
    [SerializeField] private BuildNewTowerPanel _newTowerPanel;
    [SerializeField] private StartWavePanel _startWavePanel;
    [SerializeField] private GameObject _endPanel;
    [SerializeField] private Text _resultText;

    [Inject] private GameEvents events;

    private void Start()
    {
        _newTowerPanel.gameObject.SetActive(false);
        events.OnCellClicked += HandleCellClick;
        events.OnWaveCleared += OnWaveCleaned;
        events.OnGameFinished += ShowEndPanel;
        events.OnGameStarted += ShowStartWavePanel;
    }

    private void HandleCellClick(ClickableCell cell)
    {
        if (!cell.CurrentTower)
        {
            Vector3 panelPosition = Camera.main.WorldToScreenPoint(cell.transform.position);
            _newTowerPanel.transform.position = panelPosition;
            _newTowerPanel.SaveCurrentCell(cell);
            _newTowerPanel.gameObject.SetActive(true);
            InputHandler.DisableInput();
        }
    }

    private void OnWaveCleaned(int wave)
    {
        if (wave < 2) ShowStartWavePanel();
        else ShowEndPanel(true);
    }

    private void ShowStartWavePanel()
    {
        _startWavePanel.gameObject.SetActive(true);
    }

    private void ShowEndPanel(bool win)
    {
        _endPanel.GetComponentInChildren<RestartGameButton>().SetResult(win);
        _endPanel.SetActive(true);
        _resultText.text = win ? "VICTORY!" : "Defeat";
    }
}
