using UnityEngine;
using Zenject;

public class StartMenu : MonoBehaviour
{
    [SerializeField] private GameObject startNewWaveButton;

    [Inject] private LevelManagement levelManagement;

    public void StartNewGame()
    {
        levelManagement.StartNewGame();
        ChangeUI();
    }
    public void Continue()
    {
        levelManagement.Continue();
        ChangeUI();
    }

    public void Quit()
    {
        Application.Quit();
    }

    private void ChangeUI()
    {
        gameObject.SetActive(false);
        startNewWaveButton.SetActive(true);
    }
}
