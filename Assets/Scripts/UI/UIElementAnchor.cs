using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIElementAnchor : MonoBehaviour
{
    [SerializeField] private Transform _target;
    [SerializeField] private bool _isDynamic;

    private void Start()
    {
        if (!_isDynamic) SetPosition();
    }

    private void Update()
    {
        if (!_isDynamic) return;

        SetPosition();
    }

    private void SetPosition()
    {
        transform.position = Camera.main.WorldToScreenPoint(_target.position);
    }
}
