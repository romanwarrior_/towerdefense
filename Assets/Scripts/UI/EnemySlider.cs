using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EnemySlider : MonoBehaviour
{
    [SerializeField] private Slider _slider;
    [SerializeField] private Enemy _enemy;

    private void Start()
    {
        _slider.maxValue = _enemy.MaxHealth;
    }

    private void Update()
    {
        _slider.gameObject.SetActive(_enemy.Health < _enemy.MaxHealth);
        _slider.value = _enemy.Health;
    }
}
