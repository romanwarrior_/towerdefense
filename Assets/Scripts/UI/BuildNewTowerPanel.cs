using UnityEngine;
using Zenject;

public class BuildNewTowerPanel : MonoBehaviour
{
    [SerializeField] private GameObject[] _markers;

    [Inject] private TowerBuilder _towerBuilder;

    private ClickableCell _currentCell;
    private int _currentTower;

    private void OnEnable()
    {
        _currentTower = -1;
        foreach (var marker in _markers) marker.SetActive(false);
    }

    public void SaveCurrentCell(ClickableCell cell)
    {
        _currentCell = cell;
    }

    public void Close()
    {
        gameObject.SetActive(false);
        InputHandler.EnableInput();
    }

    public void ChooseTower(int towerIndex)
    {
        _currentTower = towerIndex;
        foreach (var marker in _markers) marker.SetActive(false);
        _markers[towerIndex].SetActive(true);
    }

    public void ConfirmBuilding()
    {
        if (_currentTower == -1) 
        { 
            Close();
            return;
        }
        _towerBuilder.BuildTower(_currentCell, _currentTower);
        gameObject.SetActive(false);
        InputHandler.EnableInput();
    }
}
