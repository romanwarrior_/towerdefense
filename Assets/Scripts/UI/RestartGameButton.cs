using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Zenject;

public class RestartGameButton : MonoBehaviour
{
    [SerializeField] private Image arrow;
    [SerializeField] private Sprite restart;
    [SerializeField] private Sprite next;

    [Inject] private GameEvents events;
    [Inject] private LevelManagement levelManagement;
    private bool result;

    public void SetResult(bool result)
    {
        this.result = result;
    }

    private void OnEnable()
    {
        InputHandler.DisableInput();
        if (result)
        {
            arrow.sprite = next;
        }
        else
        {
            arrow.sprite = restart;
        }
    }

    public void Click()
    {
        transform.parent.gameObject.SetActive(false);
        if (result) levelManagement.LoadNextLevel();
        events.OnGameStarted();
    }
}
