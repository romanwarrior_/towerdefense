using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class Headquarters : MonoBehaviour
{
    [SerializeField] private float _maxHp;

    public float Hp => _hp;
    public float MaxHp => _maxHp;

    private float _hp;

    [Inject] private GameEvents events;

    private void Start()
    {
        events.OnGameStarted += RestoreHealth;
        RestoreHealth();
    }

    private void RestoreHealth()
    {
        _hp = _maxHp;
    }

    public void GetDamage(float value)
    {
        _hp -= value;
        if (_hp <= 0) Die();
    }

    private void Die()
    {
        events.OnGameFinished(false);
    }
}
