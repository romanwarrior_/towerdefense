using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float _speed;

    private Enemy _target;
    private float _damage;

    public void SetTarget(Enemy target, float damage)
    {
        _target = target;
        _damage = damage;
    }

    private void Update()
    {
        if (!_target) 
        {
            Destroy(gameObject);
            return; 
        }

        transform.position = Vector3.Lerp(transform.position, _target.transform.position, Time.deltaTime * _speed);

        float distance = Vector3.Distance(transform.position, _target.transform.position);
        if (distance <= 0.2f)
        {
            _target.GetDamage(_damage);
            Destroy(gameObject);
        }
    }
}
