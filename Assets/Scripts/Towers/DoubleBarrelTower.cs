using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoubleBarrelTower : Tower
{
    [SerializeField] private Transform[] _bulletSpawns;

    protected override void Shot()
    {
        if (!CurrentTarget) return;
        
        for (int i = 0; i < _bulletSpawns.Length; i++)
        {
            var bullet = Instantiate(_bulletPrefab, _bulletSpawns[i].position, Quaternion.identity).GetComponent<Bullet>();
            bullet.SetTarget(CurrentTarget.GetComponent<Enemy>(), _damage);
        }
    }
}
