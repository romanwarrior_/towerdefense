using UnityEngine;

public abstract class Tower : MonoBehaviour
{
    [SerializeField] protected GameObject _bulletPrefab;
    [SerializeField] protected EnemyDetector _detector;
    [SerializeField] private Transform _gun;

    [Header("Characteristics")]
    [SerializeField] protected float _attackRate;
    [SerializeField] protected float _damage;
    [SerializeField] protected float _attackRange;

    public float AttackRange => _attackRange;

    protected float TimeForOneShot => 1 / _attackRate;
    protected Transform CurrentTarget => _detector.CurrentTarget;

    protected void Start()
    {
        GetComponent<SphereCollider>().radius = _attackRange;
    }

    protected void Update()
    {
        if (_detector.CurrentTarget)
        {
            Vector3 lookDirection = Quaternion.LookRotation(_detector.CurrentTarget.position - transform.position).eulerAngles;
            _gun.transform.eulerAngles = Vector3.Lerp(_gun.transform.eulerAngles, new Vector3(0, lookDirection.y, 0), Time.deltaTime * 5);
        }
    }

    public void StartShooting()
    {
        CancelInvoke();
        InvokeRepeating(nameof(Shot), TimeForOneShot / 2, TimeForOneShot);
    }

    protected abstract void Shot();

}
