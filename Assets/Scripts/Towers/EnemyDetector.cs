using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

[RequireComponent(typeof(SphereCollider))]
public class EnemyDetector : MonoBehaviour
{
    [SerializeField] private Tower tower;

    public Transform CurrentTarget => _currentTarget;

    [Inject] private LevelManagement levelManagement;
    protected Transform _currentTarget;

    private void Start()
    {
        
    }

    protected void OnTriggerExit(Collider other)
    {
        if (_currentTarget) _currentTarget = null;
    }

    private void OnTriggerStay(Collider other)
    {
        FindClosestEnemy();
    }

    protected void FindClosestEnemy()
    {
        if (_currentTarget) return;

        float minDistace = Mathf.Infinity;
        foreach (var enemy in levelManagement.CurrentLevel.EnemySpawner.Enemies)
        {
            if (!enemy) continue;
            float distance = Vector3.Distance(transform.position, enemy.transform.position);
            if (distance <= tower.AttackRange && distance <= minDistace)
            {
                minDistace = distance;
                _currentTarget = enemy.transform;
            }
        }
            tower.StartShooting();
    }
}
