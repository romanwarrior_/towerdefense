using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SimpleTower : Tower
{
    [SerializeField] private Transform _bulletSpawn;

    protected override void Shot()
    {
        if (!CurrentTarget) return;
        
        var bullet = Instantiate(_bulletPrefab, _bulletSpawn.position, Quaternion.identity).GetComponent<Bullet>();
        bullet.SetTarget(CurrentTarget.GetComponent<Enemy>(), _damage);
    }
}
