using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class TowerBuilderInstaller : MonoInstaller
{
    [SerializeField] private TowerBuilder towerBuilder;

    public override void InstallBindings()
    {
        Container.Bind<TowerBuilder>().FromInstance(towerBuilder).AsSingle().NonLazy();
        Container.QueueForInject(towerBuilder);
    }
}
