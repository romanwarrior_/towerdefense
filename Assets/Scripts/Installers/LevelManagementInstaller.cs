using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class LevelManagementInstaller : MonoInstaller
{
    [SerializeField] private LevelManagement instance;

    public override void InstallBindings()
    {
        Container.Bind<LevelManagement>().FromInstance(instance).AsSingle().NonLazy();
        Container.QueueForInject(instance);
    }
}
