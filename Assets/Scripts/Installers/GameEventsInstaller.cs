using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Zenject;

public class GameEventsInstaller : MonoInstaller
{
    [SerializeField] private GameEvents gameEvents;

    public override void InstallBindings()
    {
        Container.Bind<GameEvents>().FromInstance(gameEvents).AsSingle().NonLazy();
        Container.QueueForInject(gameEvents);
    }
}
